# gomod-rpm-macros

The *gomod-rpm-macros* project provides files needed to automate Go (Golang) rpm
packaging based on Go modules :

 - default filesystem locations
 - architecture-specific settings
 - dependency automation (*Provides*, *Requires*, *BuildRequires*)
 - macros to simplify and standardize Go spec files, for all rpm build stages,
   including the srpm stage
 - documented templates to showcase how to use the result
 - install, pack and index Go modules

It uses [modist](https://pagure.io/modist) to analyse Go codebases.

## Licensing

*go-rpm-macros* is licensed under the GPL version 3 or later. The `spec`
templates are licensed under the MIT license.

