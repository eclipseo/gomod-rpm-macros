-- Copyright © 2017-2024 Nicolas Mailhot <nim@fedoraproject.org>,
--                       Robert-André Mauchin <zebob.m@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- SPDX-License-Identifier: GPL-3.0-or-later

-- This file contains Lua code used in rpm macros needed to create and process
-- source rpm (srpm) Go (golang) packages.
--
-- The resulting code must not use any package except redhat-rpm-macros

local   fedora = require "fedora.common"
local buildsys = require "fedora.srpm.buildsys"

local function rpmname(gomod_module, compatid)
  -- lowercase and end with '/'
  local   gomod_name = string.lower(rpm.expand(gomod_module) .. "/")
  -- remove eventual protocol prefix
  gomod_name         = string.gsub(gomod_name, "^http(s?)://",         "")
  -- remove eventual .git suffix
  gomod_name         = string.gsub(gomod_name, "%.git/*",              "")
  -- remove eventual git. prefix
  gomod_name         = string.gsub(gomod_name, "^git%.",               "")
  -- remove FQDN root (.com, .org, etc)
  -- will also remove vanity FQDNs such as "tools"
  gomod_name         = string.gsub(gomod_name, "^([^/]+)%.([^%./]+)/", "%1/")
  -- add golang prefix
  gomod_name         = "golang-" .. gomod_name
  -- compat naming additions
  local compatid = string.lower(rpm.expand(compatid))
  if  (compatid ~= nil) and (compatid ~= "") then
    gomod_name       = "compat-" .. gomod_name .. "-" .. compatid
 end
  -- special-case x.y.z number-strings as that’s an exception in our naming
  -- guidelines
  repeat
    gomod_name, i    = string.gsub(gomod_name, "(%d)%.(%d)",           "%1:%2")
  until i == 0
  -- replace various separators rpm does not like with -
  gomod_name         = string.gsub(gomod_name, "[%._/%-]+",            "-")
  -- because of the Azure sdk
  gomod_name         = string.gsub(gomod_name, "%-for%-go%-",          "-")
  -- Tokenize along - separators and remove duplicates to avoid
  -- golang-foo-foo-bar-foo names
  local result = ""
  local tokens = {}
  tokens["go"]     = true
  for token in string.gmatch(gomod_name, "[^%-]+") do
     if not tokens[token] then
        result = result .. "-" .. token
        tokens[token] = true
     end
  end
  -- reassemble the string, restore x.y.z runs, convert the vx.y.z
  -- Go convention to x.y.z as prefered in rpm naming
  result = string.gsub(result, "^-", "")
  result = string.gsub(result, ":", ".")
  -- some projects have a name that end up in a number, and *also* add release
  -- numbers on top of it, keep a - prefix before version strings
  result = string.gsub(result, "%-v([%.%d])", "-%1")
  result = string.gsub(result, "%-v([%.%d]+%-)", "-%1")
  if rpm.isdefined('go_use_new_versioning') then
    -- according to the guidelines, if the base package name does not end with
    -- a digit, the version MUST be directly appended to the package name with
    -- no intervening separator.
    -- If the base package name ends with a digit, a single underscore (_) MUST
    -- be appended to the name, and the version MUST be appended to that, in
    -- order to avoid confusion over where the name ends and the version begins.
    result = string.gsub(result, "([^-]*)(%-)([%.%d]+)$", function(prior, hyphen, version)
      if string.find(prior, "%d$") then
        return prior .. "_" .. version
      else
        return prior .. version
      end
    end)
  end
  return(result)
end

local namespace = "gomod"

local rads = {
  -- for gomod_module and gomod_devel generated packages
  ["gomod_pkg"]       = {
    ["default"]   = {"package"},
    ["computed"]  = {"name", "summary", "description", "list"},
    ["read_once"] = {"tags"} },
  -- gomod-wide radicals
  ["gomod"]         = {
    ["key"]        = {"module", "url", "forge_suffix"},
    -- gomod ←→ forge
    ["forge"]      = {"url", "patchlist", "patch_mode", "patch_level", "tag",
                      "commit", "version"},
    -- gomod → forge
    ["forge_to"]   = {"time"},
    -- forge → gomod after forge_init
    ["forge_from"] = {"setup_args", "extract_dir"},
    -- gomod → source
    ["source"]     = {"name", "summary", "epoch", "version",
                      "license", "url", "description"},
    -- from defaults
    ["default"]    = {"filters"},
    -- computed
    ["computed"]   = {"version", "version_core", "filters", "name",
                      "staging_list"},
    -- read and inherited
    ["read"]       = {"summary", "epoch", "version", "version_core",
                      "pre_version", "time", "license", "url", "description",
                      "licenses", "licenses_exclude", "docs", "docs_exclude"},
    -- read but not inherited
    ["read_once"]  = {"include"} } }

local function suffixes()
  return fedora.all_suffixes(fedora.qualify(rads[namespace]["key"], namespace))
end

local function name(suffix)
  return fedora.read(namespace .. "_name" .. suffix)
end

local function names()
  return fedora.names(name, suffixes())
end

local function floop(f, index, otherargs, verbose)
  fedora.pkg_floop(f, index, otherargs, name, suffixes(), verbose)
end

local function radicals(suffix, verbose)
  local R = {}
  for k, v in pairs(rads[namespace]) do
    R[k] = v
  end
  R["computed"] = fedora.mergelists({R["computed"], R["default"],
                                     R["forge"], R["forge_from"]})
  for _, pkg in ipairs({"module", "devel"}) do
    fedora.alias({"package"}, "default_" .. namespace .. "_" .. pkg, "",
                  namespace .. "_" .. pkg, suffix, verbose)
    if fedora.readbool("gomod_" .. pkg .. "_package" .. suffix) then
      for _, k in ipairs({"default", "computed", "read_once"}) do
        R[k] = fedora.mergelists({R[k],
                 fedora.qualify(rads[namespace .. "_pkg"][k], pkg)})
      end
    end
  end
  R["read"] = fedora.mergelists({R["default"], R["read"]})
  local all = {}
  for k, v in pairs(R) do
    all = fedora.mergelists({all, v})
  end
  R["all"] = all
  return R
end

local function init(suffix, verbose, informative)
  fedora.id('' .. namespace .. '.init("' .. suffix .. '")')
  local  forge = require "fedora.srpm.forgeng"
  local ismain = (suffix == "") or (suffix == "0")
  local myrads = radicals(suffix, verbose)
  fedora.inherit(myrads["read"], namespace, suffix, "include", verbose)
  local gomod_module_f = fedora.read(namespace .. "_url" .. suffix)
  if gomod_module_f then
    gomod_module_f = gomod_module_f:gsub("^http(s?)://", "")
    gomod_module_f = gomod_module_f:gsub("/+$",          "")
    fedora.safeset("gomod_module" .. suffix, gomod_module_f, verbose)
  end
  local forge_suffix = fedora.read("gomod_forge_suffix" .. suffix)
  if (not forge_suffix) then
    local gomod_url_f = "https://%{gomod_module" .. suffix .. "}/"
    fedora.safeset("gomod_url" .. suffix, gomod_url_f, verbose)
    forge_suffix = fedora.suffix({
      ["forge_url"]      = fedora.read("gomod_url"     .. suffix),
      ["forge_tag"]      = fedora.read("gomod_tag"     .. suffix),
      ["forge_commit"]   = fedora.read("gomod_commit"  .. suffix),
      ["forge_version"]  = fedora.read("gomod_version" .. suffix),
      }, true, verbose)
    fedora.set("gomod_forge_suffix" .. suffix, forge_suffix, verbose)
  end
  fedora.bialias(myrads["forge"],
                 "gomod", suffix, "forge", forge_suffix, verbose)
  fedora.alias(myrads["forge_to"],
                 "gomod", suffix, "forge", forge_suffix, verbose)
  forge.init(forge_suffix, verbose, informative, false)
  fedora.alias(myrads["forge_from"],
                 "forge", forge_suffix, "gomod", suffix, verbose)
  local gomod_tag = fedora.read("gomod_tag" .. suffix)
  if gomod_tag then
    local tag_p = {     version = "^v(%d+%.?%d+%.?%d+)",
                    pre_version = "^v%d+%.?%d*%.?%d*-([^-#]+)" }
    for v, p in pairs(tag_p) do
      r = gomod_tag:match(p)
      if (r ~= nil) then
        fedora.safeset("gomod_" .. v .. suffix, r, verbose)
      end
    end
  end
  fedora.mset(myrads["all"], "gomod", suffix, {
    fedora.mread(myrads["default"], "default_gomod", "", false), {
      ["version"]      = "0",
      ["version_core"] = "%{gomod_version"  .. suffix .. "}",
      ["name"]         = rpmname("%{gomod_module" .. suffix .. "}",
                                 "%{?gomod_cid"   .. suffix .. "}"),
      ["summary"]      = "The %{gomod_name" .. suffix .. "} Go software project",
      ["description"]  = "%{gomod_summary" .. suffix .. "}.",
      ["staging_list"] = "%{gomod_work_dir}/%{gomod_name" .. suffix .. "}.tmp.lst",
    }}, verbose, false)
  local gomod_name = fedora.read("gomod_name"  .. suffix)
  if fedora.readbool("gomod_module_package" .. suffix) then
    fedora.mset(myrads["all"], "gomod", suffix, {{
      ["module_name"]        = gomod_name:gsub("-module$", "") .. "-module",
      ["module_summary"]     = "The %{gomod_module" .. suffix .. "} Go module",
      ["module_description"] = [[%{expand:
%{?gomod_description]] .. suffix ..[[}

This package provides %{gomod_module]] .. suffix .. [[} Go module source code.
}]],
      ["module_list"]        = "%{gomod_work_dir}/%{gomod_module_name" .. suffix ..
                               "}.lst",
      }}, verbose, false)
  end
  if fedora.readbool("gomod_devel_package" .. suffix) then
    fedora.mset(myrads["all"], "gomod", suffix, {{
      ["devel_name"]         = gomod_name:gsub("-devel$", "") .. "-devel",
      ["devel_summary"]      = "The %{gomod_module" .. suffix ..
                               "} GOPATH source code",
      ["devel_description"]  = [[%{expand:
%{?gomod_description]] .. suffix .. [[}

This package provides %{gomod_module]] .. suffix .. [[} legacy GOPATH source code.
}]],
      ["devel_list"]         = "%{gomod_work_dir}/%{gomod_devel_name" .. suffix ..
                               "}.lst",
    }}, verbose, false)
  end
  -- Final spec variable summary if the macro was called with -i
  if informative then
    fedora.echo("Variables read or set by %%" .. namespace .. "_init")
    fedora.echovars(fedora.qualify(myrads["all"], namespace), suffix)
  end
end

local function env(suffix, verbose)
  local myrads = radicals(suffix, verbose)
  fedora.set_current(fedora.qualify(myrads["all"], namespace), suffix, verbose)
  fedora.set_verbose(verbose)
end

local function pkg(suffix, verbose)
  fedora.id('' .. namespace .. '.pkg("' .. suffix .. '")')
  local myrads = radicals(suffix, verbose)
  buildsys.reset(verbose)
  if not fedora.read("__buildsys_srpm_done") then
    fedora.alias(myrads["source"], "gomod", suffix, "source", "", verbose)
    buildsys.pkg(verbose)
  end
  if fedora.readbool(namespace .. "_module_package" .. suffix) then
    fedora.mset(buildsys.radicals, "__current_buildsys", "", {
        { ["tags"] = [[
BuildRequires:  gomod-rpm-macros
Requires:       gomod-filesystem
%{?gomod_module_tags]] .. suffix .. [[}]] },
        fedora.mread({"name", "summary", "description"},     "gomod_module",
                     suffix, false),
        fedora.mread({"epoch", "version", "url", "license"}, "gomod",
                     suffix, false),
      }, verbose, true)
    buildsys.pkg(verbose)
  end
  if fedora.readbool(namespace .. "_devel_package"  .. suffix) then
    fedora.mset(buildsys.radicals, "__current_buildsys", "", {
        { ["tags"] = [[
BuildRequires:  gomod-rpm-macros
Requires:       gomod-filesystem
%{?gomod_module_tags]] .. suffix .. [[}]]},
        fedora.mread({"name", "summary", "description"},     "gomod_devel",
                     suffix, false),
        fedora.mread({"epoch", "version", "url", "license"}, "gomod",
                     suffix, false),
      }, verbose, true)
    buildsys.pkg(verbose)
  end
end

return {
  rpmname                = rpmname,
  suffixes               = suffixes,
  names                  = names,
  floop                  = floop,
  init                   = init,
  env                    = env,
  pkg                    = pkg
}
