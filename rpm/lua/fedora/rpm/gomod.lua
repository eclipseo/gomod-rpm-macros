-- Copyright © 2017-2024 Nicolas Mailhot <nim@fedoraproject.org>,
--                       Robert-André Mauchin <zebob.m@gmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- SPDX-License-Identifier: GPL-3.0-or-later

-- This file contains Lua code used in rpm macros to create rpm packages past
-- the srpm stage: anything starting with %prep.
--
-- The sister file gomod.srpm.lua deals with srpm and buildroot setup and is more
-- restricted.

local fedora = require "fedora.common"
local  gomod = require "fedora.srpm.gomod"
local  forge = require "fedora.rpm.forgeng"

local function floop(f, index, otherargs, verbose)
  local range = gomod.suffixes(verbose)
  fedora.floop(f, index, range, otherargs)
end

local function prep(suffix, keepvendor, verbose)
  fedora.id('gomod.prep("' .. suffix .. '")')
  gomod.env(suffix, verbose)
  forge.prep(suffix, keepvendor, verbose)
  if not keepvendor then
    print(rpm.expand([[
rm -fr %{_builddir}/%{__current_gomod_extract_dir}/vendor
]]))
  end
end

local function generate_buildrequires(suffix)
  fedora.id('gomod.generate_buildrequires("' .. suffix .. '")')
  gomod.env(suffix, false)
  print(rpm.expand([[
%__gomod_generate_buildrequires
]]))
end

local function build(suffix, verbose)
  fedora.id('gomod.build("' .. suffix .. '")')
  gomod.env(suffix, verbose)
  print(rpm.expand([[
%__gomod_build
]]))
end

local function staging_dir(suffix, verbose)
  gomod.env(suffix, verbose)
  print(rpm.expand([[
%__gomod_staging_dir
]]))
end

local function install(suffix, verbose)
  fedora.id('gomod.install("' .. suffix .. '")')
  gomod.env(suffix, verbose)
  if fedora.readbool("gomod_module_package" .. suffix) then
     print(rpm.expand([[
%__gomod_module_install
]]))
  end
  if fedora.readbool("gomod_devel_package"  .. suffix) then
     print(rpm.expand([[
%__gomod_devel_install
]]))
  end
  if fedora.readbool("gomod_module_package" .. suffix) or
     fedora.readbool("gomod_devel_package"  .. suffix) then
    print(rpm.expand([[
%__gomod_other_install
]]))
  end
end

local function check(suffix, verbose)
  fedora.id('gomod.check("' .. suffix .. '")')
  gomod.env(suffix, verbose)
  fedora.set("gomodulesmode", "GO111MODULE=on")
  print(rpm.expand([[
%__gomod_check
]]))
end

local function module_files()
  print(rpm.expand('%files -n  %{__current_gomod_module_name} ' ..
                          '-f "%{__current_gomod_module_list}"\n'))
end

local function devel_files()
  print(rpm.expand('%files -n  %{__current_gomod_devel_name} ' ..
                          '-f "%{__current_gomod_devel_list}"\n'))
end

local function files(suffix, verbose)
  fedora.id('gomod.files("' .. suffix .. '")')
  gomod.env(suffix, verbose)
  if fedora.readbool("gomod_module_package"  .. suffix) then
    module_files()
  end
  if fedora.readbool("gomod_devel_package"   .. suffix) then
    print("\n")
    devel_files()
  end
end

return {
  floop                  = floop,
  prep                   = prep,
  generate_buildrequires = generate_buildrequires,
  build                  = build,
  staging_dir            = staging_dir,
  install                = install,
  check                  = check,
  files                  = files,
}
